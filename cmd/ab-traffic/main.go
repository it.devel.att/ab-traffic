package main

import (
	"log"
	"math/rand"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"ab-traffic/internal/ab-traffic/experiments"
	"ab-traffic/internal/ab-traffic/metrics"
	"ab-traffic/internal/ab-traffic/metrics/prometheus"
	"ab-traffic/internal/ab-traffic/repositories"
	"ab-traffic/internal/ab-traffic/repositories/redis"
	"ab-traffic/internal/pkg/db"
)

type server struct {
	trafficTracker  metrics.ABTrafficTracker
	experimentsRepo repositories.ExperimentsRepo
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// TODO It's not very good idea to every time make request to redis
	// it's better to store exp. in memory and update it periodically
	exampleExperiments, err := s.experimentsRepo.GetExperiment(r.Context(), "new-price-calculation")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if rand.Intn(100) < exampleExperiments.BTrafficPercent {
		exampleExperiments.Side = experiments.B
	}

	// TODO We can set cookie or header or whatever

	s.trafficTracker.IncExperiment(exampleExperiments.Name, string(exampleExperiments.Side))
	w.WriteHeader(http.StatusOK)
}

func main() {
	port := os.Getenv("PORT")
	metricPort := os.Getenv("METRIC_PORT")
	redisDSN := os.Getenv("REDIS_DSN")
	rdb := db.NewRedisClient(redisDSN)

	s := &server{
		trafficTracker:  prometheus.NewABTrafficTracker(),
		experimentsRepo: redis.NewExperimentsRepo(rdb),
	}

	metricsHandler := http.NewServeMux()
	metricsHandler.Handle("/metrics", promhttp.Handler())

	go func() {
		log.Fatal(http.ListenAndServe(port, s))
	}()
	log.Fatal(http.ListenAndServe(metricPort, metricsHandler))
}
