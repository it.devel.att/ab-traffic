# AB Traffic separator
(just minimum PoC example)

#### 1 Build backend image
```shell script
make build_image
```

#### 2 Launch docker-compose (in separate terminal or in daemon with -d flag)
```shell script
docker-compose up
```

#### 3 Put to redis some experiment (AB test experiment)
```shell script
PERCENT=20 make set_percent
```
Output
```shell script
redis-cli -p 9379 set new-price-calculation '{"name": "new-price-calculation", "b_traffic_percent": 20}'
OK
```
#### 4 Open local [grafana](http://localhost:3000/)
* login: admin 
* password: admin

#### 5 Connect prometheus to grafana
  1. Open http://localhost:3000/datasources/new?utm_source=grafana_gettingstarted
  2. Choose Prometheus
  3. As URL set `http://prometheus:9090`
  4. Click `Save & Test`
  5. All must be okay

#### 6 Add grafana dashboard
  1. Go to [dashboards](http://localhost:3000/dashboards)
  2. Click `Import`
  3. Click `Upload JSON file`
  4. Select `./ab-traffic/configs/grafana/ab-experiments traffic.json`
  5. Click Import

#### 7 Let's load some traffic on our backend
For this purpose install [wrk](https://github.com/wg/wrk)
Before load some traffic to backend let's update experiment to 10 percent on B side
```shell script
PERCENT=10 make set_percent
```
Now run wrk
```shell script
wrk -t2 -c10 -d5m http://localhost:9090
```
And let's go to our [grafana dashboard](http://localhost:3000/d/KxKms8Q7z/ab-experiments-traffic?orgId=1&refresh=5s)

You will see something like
![](./docs/img/dashboard_1.png)
> So ~10% of traffic use B side of experiment

Don't forget to restart wrk sometimes
```shell script
wrk -t2 -c10 -d5m http://localhost:9090
```
##### Lets increase B traffic to 20%
```shell script
PERCENT=20 make set_percent
```
![](./docs/img/dashboard_20_percent.png)

##### Lets increase B traffic to 33%
```shell script
PERCENT=33 make set_percent
```
![](./docs/img/dashboard_33_percent.png)

##### We can try also decrease B traffic to 5%!
```shell script
PERCENT=5 make set_percent
```
![](./docs/img/dashboard_5_percent.png)