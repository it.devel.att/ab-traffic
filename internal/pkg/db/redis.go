package db

import "github.com/go-redis/redis/v8"

func NewRedisClient(dsn string) *redis.Client {
	rdb := redis.NewClient(&redis.Options{
		Addr:     dsn,
		Password: "",
		DB:       0,
	})
	return rdb
}
