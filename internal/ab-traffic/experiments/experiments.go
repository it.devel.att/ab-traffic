package experiments

type Side string

const A Side = "A"
const B Side = "B"

type Experiment struct {
	Name            string `json:"name"`
	BTrafficPercent int    `json:"b_traffic_percent"`
	Side            Side   `json:"-"`
}
