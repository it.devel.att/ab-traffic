package metrics

type ABTrafficTracker interface {
	IncExperiment(experiment string, side string)
}
