package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"ab-traffic/internal/ab-traffic/metrics"
)

type ABTrafficTracker struct {
	experiments *prometheus.CounterVec
}

var _ metrics.ABTrafficTracker = (*ABTrafficTracker)(nil)

var labels = []string{"experiment", "side"}

func NewABTrafficTracker() *ABTrafficTracker {
	return &ABTrafficTracker{
		experiments: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "ab_experiments_counter",
				Help: "Track AB traffic experiments",
			},
			labels,
		),
	}
}

func (t *ABTrafficTracker) IncExperiment(experiment string, side string) {
	t.experiments.WithLabelValues(experiment, side).Inc()
}
