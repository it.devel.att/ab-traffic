package redis

import (
	"context"
	"encoding/json"

	"github.com/go-redis/redis/v8"

	"ab-traffic/internal/ab-traffic/experiments"
	"ab-traffic/internal/ab-traffic/repositories"
)

type ExperimentsRepository struct {
	client *redis.Client
}

var _ repositories.ExperimentsRepo = (*ExperimentsRepository)(nil)

func NewExperimentsRepo(client *redis.Client) *ExperimentsRepository {
	return &ExperimentsRepository{client: client}
}

func (repo *ExperimentsRepository) GetExperiment(ctx context.Context, id string) (experiments.Experiment, error) {
	data, err := repo.client.Get(ctx, id).Result()
	if err != nil {
		return experiments.Experiment{}, err
	}
	exp := experiments.Experiment{}
	if err := json.Unmarshal([]byte(data), &exp); err != nil {
		return experiments.Experiment{}, nil
	}
	return exp, nil
}
