package repositories

import (
	"context"

	"ab-traffic/internal/ab-traffic/experiments"
)

type ExperimentsRepo interface {
	GetExperiment(ctx context.Context, id string) (experiments.Experiment, error)
}
