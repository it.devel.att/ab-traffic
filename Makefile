
build_image:
	docker build -f ./build/ab-traffic/Dockerfile -t ab/traffic:v0.0.1 .

set_percent:
	redis-cli -p 9379 set new-price-calculation '{"name": "new-price-calculation", "b_traffic_percent": ${PERCENT}}'